import React from "react";
import ApolloClient from "apollo-boost";
import { ApolloProvider } from "@apollo/react-hooks";
import { Router } from "@reach/router";

import Layout from "./ui/Layout";
import ContactListPage from "./pages/ContactListPage";
import ContactDetailsPage from "./pages/ContactDetailsPage";
import AddContactPage from "./pages/AddContactPage";

const client = new ApolloClient()

const App = () => (
  <ApolloProvider client={client}>
    <Layout>
      <Router>
        <ContactListPage path="/" />
        <ContactDetailsPage path="/contact/:id" />
        <AddContactPage path="/add" />
      </Router>
    </Layout>
  </ApolloProvider>
);

export default App;
